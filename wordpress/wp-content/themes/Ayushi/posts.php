<?php

/**
 * Template Name: Show Category wise posts
 */
?>

<?php
$urlArray = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$segments = explode('/', $urlArray);
$numSegments = count($segments);
$currentSegment = $segments[$numSegments - 2];
$cats = get_categories();

foreach ($cats as $cat) {
    if ($cat->slug == $currentSegment) {
        $cat_id = $cat->term_id; ?>

        <div class="show">
            <?php echo "<h2><center>" . $cat->name . "</center></h2>";
            query_posts("cat=$cat_id&post_per_page=100");
            $index = 1; 
            $post_no = 0;
            ?>
            
            <div class="col-lg-12">
                <div class="row">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                   
                            <div class="col-lg-4" id="post<?php echo  $index; ?>" style="<?php echo $index == 1 ? 'display:block' : 'display:none' ?>">
                                <center><?php the_post_thumbnail(); ?></center>
                                <?php the_content(); ?>

                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                <?php echo '<hr/>'; ?>
                            </div>
                            <?php $post_no++; if($post_no % 3 == 0) $index++;  ?>
                           
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div id="loading">
            Loading Please Wait......
        </div>
    <?php } ?>
<?php }
