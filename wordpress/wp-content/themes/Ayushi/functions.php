<?php
/* Adding  css and js files */
function food()
{
    wp_enqueue_style('main', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('custom', get_template_directory_uri() . '/assets/css/custom.css');
    wp_enqueue_style('menus-css', get_template_directory_uri() . '/assets/css/menus.css');
    wp_enqueue_style('font-awesome',  'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('font-style', 'https://fonts.googleapis.com/css2?family=Bellota+Text:ital,wght@0,400;0,700;1,300;1,400;1,700&family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400&display=swap');
    wp_enqueue_style('wp-scipts', 'https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css');
    wp_enqueue_script('wp-script', 'https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js');
    wp_enqueue_script('wp-script-js', 'https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js');
    wp_enqueue_style('slick_css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css');
    wp_enqueue_style('slick_theme_css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css');
    wp_enqueue_script('slick-jquery', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js');
    wp_enqueue_script('custom-jquery', (get_stylesheet_directory_uri() . '/assets/js/slick.js'));
    wp_enqueue_script('menus-js', get_template_directory_uri() . '/assets/js/menus.js');
    wp_enqueue_script('scroll-js', (get_stylesheet_directory_uri() . '/assets/js/scroll.js'));
}
add_action("wp_enqueue_scripts", "food");
add_theme_support('post-thumbnails'); 
function wpb_custom_new_menu()
{
    register_nav_menu('footer-menu', __('Footer Menu'));
    register_nav_menu('header-menu', __('Header Menu'));
}
add_action('init', 'wpb_custom_new_menu');
require get_template_directory().'/inc/category-image/category_image.php'; /* To add Category Image*/
require get_template_directory().'/inc/breadcrumb/breadcrumb.php';    /* To add Breadcrumb*/
function mytheme_custom_excerpt_length( $length ) {
    return 35;
}
add_filter( 'excerpt_length', 'mytheme_custom_excerpt_length');

?>