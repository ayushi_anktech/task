$(document).ready(function(){
    $('.latest-recipes__slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        dots: true,
        infinite: true,
        autoplay: true,
      })
    

    // ( function( $ ) {
    //     class SlickCarousel {
    //       constructor() {
    //         this.initiateCarousel();
    //       }
      
    //       initiateCarousel() {
    //         $( '.posts-carousel' ).slick( {
    //           autoplay: true,
    //           autoplaySpeed: 1000,
    //           slidesToShow: 3,
    //           slidesToScroll: 1,
    //         } );
    //       }
    //     }
      
    //     new SlickCarousel();
      
    //   } )( jQuery );
});