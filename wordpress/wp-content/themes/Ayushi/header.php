<!DOCTYPE html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?php wp_head(); 
  $Searchdata='';
  if($_GET['title']!=''){
    $Searchdata=$_GET['title'];
  }
   ?>

</head>

<body>
  <header class="header">
    <div class="header-container">
      <div class="row header-inner">
        <div class="col-lg-4">
          <div class="header-left">
            <a id="menu-toggle" href="#" class="toggle"> <img src="<?php echo get_template_directory_uri() ?>/img/menu.png"></a>
            <div id="sidebar-wrapper">
              <form method="get">
                <div class="from-group" > 
                  <input placeholder="search recepies" name="title"  type="search" id="site-search" >
                  <button type="submit">Search</button>
                </div>
              </form>
                <a id="menu-close" href="#" class="btn btn-default btn-lg pull-right toggle"><i class="fa fa-close"></i></a>
              <ul class="sidebar-nav">
                <li class="sidebar-brand">
                  <?php
                  wp_nav_menu(array(
                    'theme_location' => 'header-menu'
                  )); ?>
                   
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="header-logo">
            <a href="<?php home_url(); ?>"><img src="<?php echo get_template_directory_uri() ?>/img/mylogo.jpg"></a>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="header-right">
            <div class="header-subscribe">
              <a href="#">SUBSCRIBE</a>
            </div>
            <div class="header-search-icon">
              <a id="menu-toggles" class="toggle" href="#"><img src="<?php echo get_template_directory_uri() ?>/img/search-interface-symbol.png"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>