<?php
 
if(!function_exists('filter_breadcrumb_trail')){
    function filter_breadcrumb_trail($output)
    {
        echo '<a href="' . home_url() . '" rel="nofollow">Home</a>';
            if (is_category() || is_single()) {
                echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
                the_category(' &bull; ');
                if (is_single()) {
                        echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
                        $output .= the_title();
                    }
                } 
                return $output;  
            };
            add_filter('breadcrumb_trail', 'filter_breadcrumb_trail'); 
        }
?>