<?php
/*
  Template Name:Home
 */
 get_header(); ?>

<div id="carousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel" data-slide-to="0" class="active"></li>
    <li data-target="#carousel" data-slide-to="1"></li>
    <li data-target="#carousel" data-slide-to="2"></li>
  </ol> <!-- End of Indicators -->

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <img src="<?php echo get_template_directory_uri() ?>/img/with-logo-1.jpg" alt="Los Angeles" style="width:100%;height:500px">
    </div>

    <div class="carousel-item">
      <img src="<?php echo get_template_directory_uri() ?>/img/with-logo-1.jpg" alt="Chicago" style="width:100%;height:500px">
    </div>

    <div class="carousel-item">
      <img src="<?php echo get_template_directory_uri() ?>/img/with-logo-1.jpg" alt="New york" style="width:100%;height:500px">
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<center>
  <H1>Browse by Category</H1>
</center>

  <div class="container" style="margin-top:2%;">
    <div class="row">
    
        <?php
        $cats = get_categories();
  
        foreach ($cats as $cat) { ?>
         
          <div class="col-lg-3 col-md-3 col-sm-6">
            <?php 
          $image_id = get_term_meta ( $cat->term_id, 'image_id', true );
          echo "<a href=".home_url()."/index.php/category/".$cat->slug.">". wp_get_attachment_image ( $image_id, 'full' )."</a>";
          echo "<center><a href=".home_url()."/index.php/category/".$cat->slug."><h3>".$cat->name."</h3></a></center>";?>
         
         </div>
         <?php
        }
        ?>
    </div>

  </div>
  
<center>
  <H1>Latest Post</H1>
</center>

<?php
$the_query = new WP_Query(array('posts_per_page' => 3));
if ($the_query->have_posts()) :
  $the_query->the_post();
?>
  <div class="container" style="margin-top:2%;">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-6">
      

        <?php the_post_thumbnail(); ?>
        <center>
          <h3 style="font-family: 'Courier New', Courier, monospace;"><?php the_title(); ?></h3>
        </center>
        <h6><?php the_meta(); ?> </h6>
      </div>

      <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
        <div class="col-lg-4 col-md-4 col-sm-6">
        

          <?php the_post_thumbnail(); ?>
          <center>
            <h3 style="font-family: 'Courier New', Courier, monospace;"><?php the_title(); ?></h3>
          </center>
          <h6><?php the_meta(); ?> </h6>
        </div>
      <?php endwhile; ?>

    </div>

  </div>
  <div class="text-center">
    <button type="button" class="btn btn-primary">show More</button>
  </div>
  <!--end grid-->
<?php endif; ?>

<?php get_footer(); ?>