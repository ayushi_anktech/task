<?php
/*
    Template Name: subscribe
*/
get_header();
?>

<section class="food-story">
    <div class="container">
        <div class="food-story_left">
            <div class="row">
                <div class="col-lg-8">
                <h1 style="font-weight:bold"><?php the_title(); ?></h1>
                  <div class="myform">
                    <?php the_post_thumbnail(); ?>
                    <h4>5 Tips to Amazing Indian Food</h4>
                    <p>Get my secrets to restaurant style food!</p>
                    <form action="#" class="form_content">
                        <input type="text" class="mb-2" name="name" placeholder="name">
                        <br>
                        <input type="text" class="mb-3" name="E-mail" placeholder="E-mail">
                        <br>
                        <button type="submit" class="submit_button">Yes! Send them my way</button>
                    </form>

                </div>
                </div>


                <!-- End left section -->

                <!-- Start right section -->
                <div class="col-lg-4">
                    <!-- Start persion details section -->
                    <div class="row">
                        <div class="col-lg-12 right_content">
                            <div class="right_image">
                                <img src="<?php echo get_template_directory_uri() ?>/img/mobile-top.jpg">
                            </div>
                            <p class="text-center"><strong>I am rakhi</strong> the cook, writer and photographer
                                behind this little
                                blog. I’ve grown up in the kitchen along side my mum and grandmothers and
                                conversations in my family are always about the next meal. I’ve picked up their love
                                for food along the way, and with this blog, I share my food story with you.
                            </p>
                        </div>
                    </div>
                    <!-- End persion details section -->

                    <!-- Start top recipes section -->
                    <div class="top-recipes">
                        <h2 class="mb-4">Top Recipes</h2>
                        <div class="row">
                            <?php
                            $the_query = new WP_Query(array('posts_per_page' => 4)); ?>

                            <?php if ($the_query->have_posts()) :
                                while ($the_query->have_posts()) : $the_query->the_post(); ?>
                                    <div class="col-lg-6 common_class">
                                        <?php the_post_thumbnail(); ?>
                                        <h4 class="h4_heading" style="color: #630460"><?php the_title(); ?></h4>

                                    </div>
                                <?php endwhile ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- End top recipes section -->
                    <!-- Start christmas favourites sectoin -->
                    <div class="christmas-favourites">
                        <h2 class="mb-4">Christmas Favourites</h2>
                        <div class="row">
                            <?php
                            $the_query = new WP_Query(array('posts_per_page' => 6)); ?>

                            <?php if ($the_query->have_posts()) :
                                while ($the_query->have_posts()) : $the_query->the_post(); ?>
                                    <div class="col-lg-6 common_class">
                                        <?php the_post_thumbnail(); ?>
                                        <h4 class="h4_heading" style="color: #630460"> <?php the_title(); ?></h4>

                                    </div>
                                <?php endwhile ?>
                            <?php endif; ?>

                        </div>
                    </div>
                    <!-- End christmas favourites sectoin -->
                    <!-- End right section -->
                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
?>