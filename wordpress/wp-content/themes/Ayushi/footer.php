<div class="site_footer">
    <!-- Footer -->
    <footer class="text-center text-lg-start bg-light text-muted">
        <section class="">
            <div class="container text-center text-md-start mt-5">
                <!-- Grid row -->
                <div class="row mt-3">
                    <!-- Grid column -->
                    <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-4">
                        <!-- Content -->
                        <h6 class="text-uppercase fw-bold mb-4">
                            <i class="fa fa-gem me-3"></i>Company name
                        </h6>
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'my-custom-menu'
                        ));
                        ?>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xl-4 mx-auto mb-4">
                        <!-- Content -->
                        <h6 class="text-uppercase fw-bold mb-4">
                            <i class="fa fa-gem me-3"></i>Social Networks
                        </h6>
                        <a href="" class="me-4 text-reset">
                            <i class="fa fa-facebook-f"></i>
                        </a>
                        <a href="" class="me-4 text-reset">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="" class="me-4 text-reset">
                            <i class="fa fa-google"></i>
                        </a>
                        <a href="" class="me-4 text-reset">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <a href="" class="me-4 text-reset">
                            <i class="fa fa-linkedin"></i>
                        </a>
                        <a href="" class="me-4 text-reset">
                            <i class="fa fa-github"></i>
                        </a>
                    </div>

                    <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                        <!-- Links -->
                        <h6 class="text-uppercase fw-bold mb-4">
                            Contact
                        </h6>
                        <p><i class="fa fa-home me-3"></i> New York, NY 10012, US</p>
                        <p>
                            <i class="fa fa-envelope me-3"></i>
                            info@example.com
                        </p>
                        <p><i class="fa fa-phone me-3"></i> + 01 234 567 88</p>
                       
                    </div>
                    <!-- Grid column -->
                </div>
                <!-- Grid row -->
            </div>
        </section>
        <!-- Section: Links  -->

        <!-- Copyright -->
        <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
            © 2021 Copyright:
            <a target="_blank" class="text-reset fw-bold" href="https://www.anktech.co.in">AnkTech Softwares</a>
        </div>
        <!-- Copyright -->
    </footer>
    <!-- Footer -->
</div>
</body>