<?php
/*
    Template Name:menus
    */
get_header();
?>

    <div class="container" >
        
            <center><h1 ><?php the_title(); ?> </h1></center>
       
        <?php if (have_rows('food_item')) :  ?>
            <div class="container"style="margin-top:5%;">
                <div class="row">
           
                <?php while (have_rows('food_item')) : the_row();
                    $name = get_sub_field('name');
                    $bio = get_sub_field('content');
                    $image = get_sub_field('image');
                    $pic = $image['sizes']['thumbnail'];
                ?>
                 <div class="col-sm">
                    <center><h4><?php echo $name;  ?></h4></center>
                   
                    <?php if ($image) : ?>
                        <img style="width:300px;height:200px" src="<?php echo $pic; ?>">
                    <?php endif ?>
                    <p><?php echo $bio;  ?></p>
                </div>
                           
                <?php endwhile; ?>
        </div>
        </div> 
           
        <?php endif; ?>

    </div>

<?php

get_footer();
?>