<?php
   
    function food(){
       
        wp_register_style('style',get_template_directory_uri().'-child/style.css', array(),'1.0');
        
        wp_enqueue_style('normalize');
        wp_enqueue_style('style');
        wp_enqueue_style('fontawesome');
    }
    add_action("wp_enqueue_scripts","food");
    
?>