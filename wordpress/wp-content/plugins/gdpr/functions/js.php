<?php 
     function frontend_js(){
        
        wp_enqueue_script('frontend-gdpr',plugins_url('../html/js/jquery.js', __FILE__));
        wp_enqueue_script('google-hosted',plugins_url('../html/js/gdpr.js', __FILE__));
    }
    add_action("wp_footer","frontend_js");
    function backend_js(){
  
      wp_enqueue_script('index1',plugins_url('../html/js/jquery.js', __FILE__));
      wp_enqueue_script('index',plugins_url('../html/js/gdpr-index.js', __FILE__));
  }
  add_action("admin_head","backend_js");
 ?>