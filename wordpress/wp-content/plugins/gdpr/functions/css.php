<?php
    function backend_css(){
        echo "<style>";
        include(plugin_dir_path(__DIR__).'/html/css/backend.css');
    }
    add_action("admin_head","backend_css");
    function frontend_css(){
        
        wp_enqueue_style('frontend-gdpr',plugins_url('../html/css/gdpr-style.css', __FILE__));
    }
    add_action("wp_head","frontend_css");
?>