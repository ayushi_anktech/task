<?php
/*
    plugin Name:  GDPR
    Description:  provide a GDPR function  to our website
    Version:      1.0
    Author:       Aayushi Ahuja
 */
    defined ('ABSPATH') or die ('Page not found');
    ob_start();
    if(file_exists(dirname(__FILE__).'/functions/css.php')){
        
        require_once dirname(__FILE__).'/functions/css.php';     
    }
    if(file_exists(dirname(__FILE__).'/functions/html.php')){
        
        require_once dirname(__FILE__).'/functions/html.php';     
    }
    if(file_exists(dirname(__FILE__).'/functions/js.php')){
        
        require_once dirname(__FILE__).'/functions/js.php';     
    }
    ob_clean();
if ( !class_exists( 'My_Plugin' ) ) {  
    class GDPR {
        public function __construct() {
           
			register_activation_hook( __FILE__, array( $this, 'gdpr_activation_hook' ) );         // Plugin activation hook
			register_deactivation_hook( __FILE__, array( $this, 'gdpr_deactivation_hook' ) );     // Plugin deactivation hook      // Plugin uninstall hook
			add_action( 'admin_menu',array( $this, 'gdpr_plugin_menu') );
            $plugin = plugin_basename(__FILE__); 
            add_filter( "plugin_action_links_$plugin", array( $this, 'settings_link' ) );
			//add_action( 'admin_enqueue_scripts', array($this, 'all_scripts') );
            //add_action('wp_enqueue_scripts', array(&$this, 'front_end_scripts'));
		}

        function gdpr_activation_hook(){
            
            global $wpdb;

            # name of the table
            $table=$wpdb->prefix.'gdpr';

            # check for table in the data
            $check_table=$wpdb->get_results("SHOW TABLES LIKE '".$table."' ");

            # count the number of table that are return by check_table
            if(count($check_table) == 0){

                $create_table=$wpdb->get_results("create table ".$table."(ID INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, ip_address VARCHAR(32) NULL  , userID INT(11)  DEFAULT 0,  date VARCHAR(19)  NULL, reg_date TIMESTAMP , INDEX(userID))");

            } # end the check table count
        }
    

        function gdpr_plugin_menu() {
            add_menu_page('GDPR Cookie Consent', 'GDPR Cookie Consent', 'manage_options', 'menu-page', array( $this, 'menu_page'), 'dashicons-privacy');
            add_submenu_page('menu-page', 'submenu item', 'GDPR Settings', 'manage_options', 'menu-page', array( $this, 'menu_page'));
            add_submenu_page('menu-page', 'submenu item', 'Cookie List', 'manage_options', 'cookie-list-page', array( $this, 'cookie_list'));
            add_submenu_page('menu-page', 'submenu item', 'Policy generator', 'manage_options', 'policy', array( $this, 'Policy_generator'));
            add_submenu_page('menu-page', 'submenu item', 'Privacy Overview', 'manage_options', 'privacy', array( $this, 'privacy_overview'));
           
        }
        public function settings_link( $links ) {
			$settings_link = "<a href='admin.php?page=menu-page'>Settings</a>";
			array_push( $links, $settings_link );
			return $links;
        }   
        function menu_page() {
			echo $this->set_template('settings-page');
		}
        function privacy_overview(){
            echo $this->set_template('privacy_overview');
        }
        function Policy_generator(){
            echo $this->set_template('policy');
        }

		function cookie_list() {
			echo $this->set_template('cookies-list');
		}
        
        function set_template( $aTemplate ) {
			ob_start();
			include_once "html/{$aTemplate}.php";
			return ob_get_clean();
		}
    }
}
if ( class_exists( 'GDPR' ) ) {
	$gdpr = new gdpr;
}
?>