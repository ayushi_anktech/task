<div class="wrap gdpr">
    <h2>GDPR Configuration</h2>
    <?php
    if (isset($_POST['save_gdpr_settings']) &&  empty($_POST['gdpr_text'])) {
        echo "<script>alert('please fill the cookie content')</script>";
    }
    if (isset($_POST['save_gdpr_settings']) &&  !empty($_POST['gdpr_text'])) {
        update_option('update_gdpr_text', $_POST['gdpr_text']);

        update_option('update_gdpr_button_text', $_POST['gdpr_button_text']);
        update_option('enable_privacy_policy_link', $_POST['enable_privacy_policy_link']);
        if (isset($_POST['privacy_policy_page_link_checkbox'])) {
            //echo "<script>alert('string')</script>";
            update_option('privacy_policy_page_link_option', $_POST['privacy_policy_page_link_option']);
            update_option('privacy_policy_page_link_checkbox', true);
        } elseif (!(isset($_POST['privacy_policy_page_link_checkbox']))) {
            //echo "<script>alert('stringfalse')</script>";
            update_option('privacy_policy_page_link_checkbox', false);
        }
        if (isset($_POST['custom-url-input-radio-name']) && $_POST['custom-url-input-radio-name'] == "custom") {
            update_option('privacy_policy_page_link_option', $_POST['privacy_policy_custom_link_option']);
        }

        if (isset($_POSTcookie_notice_options_effect['barColor'])) {
            update_option('barColor', $_POST['barColor'], true);
        }

        if (isset($_POST['cookie_notice_options_position']) && $_POST['cookie_notice_options_position'] == "top" || $_POST['cookie_notice_options_position'] == "bottom") {
            update_option('cookie_notice_options_position', $_POST['cookie_notice_options_position']);
        }
        if (isset($_POST['cookie_notice_options_effect']) && $_POST['cookie_notice_options_effect'] == "none" || $_POST['cookie_notice_options_effect'] == "fade" || $_POST['cookie_notice_options_effect'] == "slide") {
            update_option('cookie_notice_options_effect', $_POST['cookie_notice_options_effect']);
        }

        if (isset($_POST['cn_on_scroll_offset_checkbox'])) {
            update_option('cn_on_scroll_offset_name',  $_POST['cn_on_scroll_offset_name'], true);
            update_option('cn_on_scroll_offset_checkbox',  true, yes);
        } elseif (!(isset($_POST['cn_on_scroll_offset_checkbox']))) {
            update_option('cn_on_scroll_offset_checkbox', false, yes);
        }
        if (isset($_POST['cn_refuse'])) {
            update_option('cookie_notice_refuse',  $_POST['cookie_notice_refuse'], true);
            update_option('cn_refuse',  true, yes);
        } elseif (!(isset($_POST['cn_refuse']))) {
            update_option('cn_refuse', false, yes);
        }
        if ($_POST['cookie_notice_deactivatoin'] == "checked") {

            global $wpdb;
            $table = $wpdb->prefix . 'gdpr';
            $wpdb->query("DROP TABLE IF EXISTS " . $table);
            global $wpdb;
            $table = $wpdb->prefix . 'gdpr';
            $check_table = $wpdb->get_results("SHOW TABLES LIKE '" . $table . "' ");
            if (count($check_table) == 0) {
                $create_table = $wpdb->get_results("create table " . $table . "(ID INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY, ip_address VARCHAR(32) NULL  , userID INT(11)  DEFAULT 0,  date VARCHAR(19)  NULL, reg_date TIMESTAMP , INDEX(userID))");
            }
        }
       
        if (isset($_POST['cookie_notice_reloading'])) {
            //echo "<script>alert('string')</script>";
            update_option('cookie_notice_reloading', true, yes);
        } elseif (!(isset($_POST['cookie_notice_reloading']))) {
            update_option('cookie_notice_reloading', false, yes);
        }  
             
         
       
         update_option('cookie_notice_link', $_POST['cookie_notice_link']);
       
    ?>
        <div id="setting-error-settings_updated" class="notice notice-success settings-error is-dismissible">
            <p><strong>GDPR Settings have been updated.</strong></p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
        </div>
    <?php
    } ?>

    <?php $gdpr_text = get_option('update_gdpr_text', '');
    $gdpr_button_text = get_option('update_gdpr_button_text', '');
    $enable_privacy_policy_link = get_option('enable_privacy_policy_link', '');
    $privacy_policy_page_link_option = get_option('privacy_policy_page_link_option', '');
    $privacy_policy_page_link_checkbox = get_option('privacy_policy_page_link_checkbox', '');
    $cookie_notice_options = get_option('cookie_notice_options', '');
    $textColor = get_option('textColor', '');
    $barColor = get_option('barColor', '');
    $cookie_notice_options_position = get_option('cookie_notice_options_position', '');
    $cookie_notice_options_effect = get_option('cookie_notice_options_effect');
    $cn_on_scroll_offset_name = get_option('cn_on_scroll_offset_name', '');
    $cn_on_scroll_offset_checkbox = get_option('cn_on_scroll_offset_checkbox', '');
    $cookie_notice_reloading = get_option('cookie_notice_reloading');
    
    $cn_refuse = get_option('cn_refuse', '');
    $cookie_notice_refuse = get_option('cookie_notice_refuse','');
    $cookie_notice_link =get_option('cookie_notice_link','');
    echo "link=".$cookie_notice_link;
    $pages = get_pages(
        array(
            'sort_order'    => 'ASC',
            'sort_column'   => 'post_title',
            'hierarchical'  => 0,
            'child_of'      => 0,
            'parent'        => -1,
            'offset'        => 0,
            'post_type'     => 'page',
            'post_status'   => 'publish'
        )
    );
    //var_dump($pages);
    ?>

    <form action="" method="post">
        <table class="form-table" role="presentation">
            <tbody>
                <tr>
                    <th scope="row">Message</th>
                    <td>
                        <fieldset>
                            <div id="cn_message_text">
                                <textarea name="gdpr_text" class="large-text" cols="50" rows="5" placeholder="GDPR Frontend Text"><?php echo $gdpr_text; ?></textarea>
                                <p class="description">Enter the cookie message.</p>
                            </div>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Button text</th>
                    <td>
                        <fieldset>
                            <div id="cn_accept_text">
                                <input placeholder="GDPR Button Text" type="text" class="widefat" name="gdpr_button_text" value="<?php echo  $gdpr_button_text; ?>">
                                <p class="description">The text of the option to accept the usage of the cookies and make the notification disappear.</p>
                            </div>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Privacy policy</th>
                    <td>
                        <fieldset>
                            <label style="font-size:15px ;font-weight:normal;"><input id="cn_see_more" type="checkbox" name="privacy_policy_page_link_checkbox" <?php echo $privacy_policy_page_link_checkbox ? 'checked' : ''; ?>>Enable privacy policy link.</label>
                            <div class="privacy-data" style="display: none;">
                                <div id="cn_see_more_opt" style="display: block;">
                                    <input type="text" class="regular-text" name="enable_privacy_policy_link" value="<?php echo $enable_privacy_policy_link; ?>" placeholder="Privacy Policy">
                                    <p class="description">The text of the privacy policy button.</p>
                                </div>
                                <div id="cn_see_more_opt_custom_link">
                                    <label style="font-size:15px ;font-weight:normal;"><input id="page-url-input-radio" type="radio" name="custom-url-input-radio-name" value="page" checked="checked">Page link</label>
                                    <label style="font-size:15px ;font-weight:normal;"><input id="custom-url-input-radio" type="radio" name="custom-url-input-radio-name" value="custom">Custom link</label>
                                </div>
                                <p class="description">Select where to redirect user for more information about cookies.</p>
                                <div class="cn_see_more_opt_page-class" style="display: block;">
                                    <select name="privacy_policy_page_link_option">
                                        <option value="" selected="selected">-- Select Page --</option>
                                        <?php
                                        if ($pages) {
                                            foreach ($pages as $page) {
                                                $selected = $privacy_policy_page_link_option && $privacy_policy_page_link_option == $page->guid ? "selected" : "";
                                                echo '<option ' . $selected . ' value="' . $page->guid . '" >' . esc_html($page->post_title) . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                    <p class="description">Select from one of your site's pages.</p>
                                </div>
                                <div class="custom-url-input-cls" id="custom-url-input-id" style="display: none;">
                                    <input type="text" class="regular-text" name="privacy_policy_custom_link_option" value="<?php echo $privacy_policy_page_link_option ?>" placeholder="https://www.google.com/" />
                                    <p class="description">Enter the full URL starting with http(s)://</p>
                                </div>
                                <div id="cn_see_more_opt_link" style="display: none;">
                                    <input type="text" class="regular-text" name="cookie_notice_options[see_more_opt][link]" value="">
                                    <p class="description">Enter the full URL starting with http(s)://</p>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Link target</th>
                    <td>
                        <fieldset>
                            <div id="cn_link_target">
                                <select name="cookie_notice_link">
                                    <option value="_blank" selected="<?php echo $cookie_notice_link && $cookie_notice_link == "_blank" ? "selected" : "";  ?>">_blank</option>
                                    <option value="_self"selected<?php echo $cookie_notice_link && $cookie_notice_link == "_blank" ? "selected" : "";  ?>>_self</option>
                                </select>
                                <p class="description">Select the privacy policy link target.</p>
                            </div>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Refuse cookies</th>
                    <td>
                        <fieldset>
                            <label style="font-size:15px ;font-weight:normal;"><input id="cn_refuse" type="checkbox" name="cn_refuse" <?php echo $cn_refuse ? 'checked': '' ; ?>>Enable to give to the user the possibility to refuse third party non functional cookies.</label>
                            <div class="cn_refuse_opt_container" style="display: none;">
                                <div id="cn_refuse_text">
                                    <input type="text" class="regular-text" name="cookie_notice_refuse" value="<?php echo $cookie_notice_refuse;  ?>" placeholder="Refuse">
                                    <p class="description">The text of the button to refuse the usage of the cookies.</p>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Reloading</th>
                    <td>
                        <fieldset>
                        
                            <label style="font-size:15px ;font-weight:normal;">
                            <div class="reloading-data" style="display: block;"><input id="cn_redirection" type="checkbox" name="cookie_notice_reloading" <?php echo $cookie_notice_reloading ? 'checked' : ''; ?>>Enable to reload the page after cookies are accepted.</label>
                        </div>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">On scroll</th>
                    <td>
                    <fieldset>
                        <label style="font-size:15px ;font-weight:normal;"><input id="cn_on_scroll" type="checkbox" name="cn_on_scroll_offset_checkbox" <?php echo $cn_on_scroll_offset_checkbox ? 'checked': '' ; ?>>Enable cookie notice acceptance when users scroll.</label>
                        <div class="cn_on_scroll_offset-cls" id="cn_on_scroll_offset" style="display: none;">
                        <input type="text" name="cn_on_scroll_offset_name"  value="<?php echo $cn_on_scroll_offset_name;?>"> <span>px</span>
                        <p class="description">Number of pixels user has to scroll to accept the usage of the cookies and make the notification disappear.</p></div>
                    </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Cookie expiry</th>
                    <td>
                        <fieldset>
                            <div id="cn_time">
                                <select name="cookie_notice_expiry">
                                    <option value="hour">An hour</option>
                                    <option value="day">1 day</option>
                                    <option value="week">1 week</option> selected='selected'
                                    <option value="month" selected="selected">1 month</option>
                                    <option value="3months">3 months</option>
                                    <option value="6months">6 months</option>
                                    <option value="year">1 year</option>
                                    <option value="infinity">infinity</option>
                                </select>
                                <p class="description">The amount of time that cookie should be stored for.</p>
                            </div>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <th scope="row">Deactivation</th>
                    <td>
                        <fieldset>
                            <label style="font-size:15px ;font-weight:normal;"><input id="cn_deactivation_delete" type="checkbox" name="cookie_notice_deactivatoin" value="checked">Enable if you want all plugin data to be deleted on deactivation.</label>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
        <h2>Design</h2>
        <table class="form-table" role="presentation">
            <tbody>
                <tr>
                    <th scope="row">Position</th>
                    <td>
                        <fieldset>
                            <div id="cn_position">
                                <label style="font-size:15px ;font-weight:normal;"><input id="cn_position-top" type="radio" name="cookie_notice_options_position" value="top" <?php echo ($cookie_notice_options_position == 'top') ? 'checked' : ''; ?>>Top</label>
                                <label style="font-size:15px ;font-weight:normal;"><input id="cn_position-bottom" type="radio" name="cookie_notice_options_position" value="bottom" <?php echo ($cookie_notice_options_position == 'bottom') ? 'checked' : ''; ?>>Bottom</label>
                                <p class="description">Select location for your cookie notice.</p>
                            </div>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <th scope="row">Animation</th>
                    <td>
                        <fieldset>
                            <div id="cn_hide_effect">
                                <label style="font-size:15px ;font-weight:normal;"><input id="cn_hide_effect-none" type="radio" name="cookie_notice_options_effect" value="none" <?php echo ($cookie_notice_options_effect == 'none') ? 'checked' : ''; ?>>None</label>
                                <label style="font-size:15px ;font-weight:normal;"><input id="cn_hide_effect-fade" type="radio" name="cookie_notice_options_effect" value="fade" <?php echo ($cookie_notice_options_effect == 'fade') ? 'checked' : ''; ?>>Fade</label>
                                <label style="font-size:15px ;
                                        font-weight:normal;"><input id="cn_hide_effect-slide" type="radio" name="cookie_notice_options_effect" value="slide" <?php echo ($cookie_notice_options_effect == 'slide') ? 'checked' : ''; ?>>Slide</label>
                                <p class="description">Cookie notice acceptance animation.</p>
                            </div>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Colors</th>
                    <td>
                        <fieldset>
                            <input type="color" id="colorpicker" name="textColor"  value="<?php echo $textColor; ?>">
                            <div id="cn_colors-text"><label style="font-size:15px ;font-weight:normal;">Text color</label><br>
                                <input type="color" id="colorpicker" name="barColor" value="<?php echo  $barColor; ?>">
                                <div id="cn_colors-bar"><label style="font-size:15px ;font-weight:normal;">Bar color</label>
                        </fieldset>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="button_container">
            <input type="submit" name="save_gdpr_settings" id="save" class="button button-primary " value="SAVE GDPR SETTINGS">
            <input type="submit" name="reset_cookie_notice_options" id="reset_cookie_notice_options" class="button" value="Reset to defaults">
        </div>
    </form>
</div>