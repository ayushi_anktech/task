$( document ).ready(function() {

    $('#cn_see_more').on("change",function() { 
        $('.privacy-data').toggle(this.checked); // toggle instead
    }).change(); // trigger the change
    
    $('#cn_refuse').on("change",function() { 
        $('.cn_refuse_opt_container').toggle(this.checked); // toggle instead
    }).change(); // trigger the change

    $('#cn_on_scroll').on("change",function() { 
        console.log('scrolling...');
        $('.cn_on_scroll_offset-cls').toggle(this.checked);
    }).change();

    $('#custom-url-input-radio').on("click",function() { 
        $('.custom-url-input-cls').css("display", "block");
        $('.cn_see_more_opt_page-class').css("display", "none");
    });

    $('#page-url-input-radio').on("click",function() { 
        $('.custom-url-input-cls').css("display", "none");
        $('.cn_see_more_opt_page-class').css("display", "block");
    });
   
});