<div class="wrap gdpr">
    
    <?php
    global $wpdb, $table_prefix;
    $table = $table_prefix . 'gdpr';
    $q = "select * from   $table";

    $result = $wpdb->get_results($q);
    //$get = $wpdb->get_results("select * from ".$wpdb->prefix."gdpr order by ID DESC");
    ?>
    <table class="wp-list-table widefat fixed striped table-view-list posts">
        <thead>
            <tr>
                <th>ID</th>
                <th>IP ADDRESS</th>
                <th>UserID</th>
                <th>Date</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($result as $row) :
            ?>
                <tr>
                    <td><?php echo $row->ID; ?></td>
                    <td><?php echo $row->ip_address; ?></td>
                    <td><?php echo $row->userID; ?></td>
                    <td><?php echo $row->date; ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>