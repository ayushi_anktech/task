
<div   class="gdpr_container" style="<?php echo get_option('cookie_notice_options_position').':0 ;' ?>"  id="ffff <?php echo get_option('cookie_notice_options_effect'); ?>" data-url="<?php echo plugin_dir_url(__DIR__); ?>accept.php">
    <h2 style="color:black">Your Privacy</h2>
      <div class="image">
      <img src="<?php echo plugin_dir_url( dirname( __FILE__) ) . 'images/cookies1.png'; ?>">
      </div>
      <div class="gdpr_text_content" style="<?php echo 'background-color:'.get_option(' barColor'); ?>">
        <p style="<?php  echo 'font-size:15px;color:'.get_option('textColor'); ?>"><?php echo get_option('update_gdpr_text','we use cookies to make your experience with this site better'); ?></p>
        <button class="gdpr_button" id="gdpr_accept"><?php echo get_option('update_gdpr_button_text','ACCEPT'); ?>
        </button>
        <input type="hidden" id="scroll" value="<?php echo get_option('cn_on_scroll_offset_name'); ?>">
        <input type="hidden" id="scroll_checkbox" value="<?php echo get_option('cn_on_scroll_offset_checkbox'); ?>">
        <input type="hidden" id="reload_on_click" value="<?php echo get_option('cookie_notice_reloading'); ?>">
        
        <?php if(get_option('privacy_policy_page_link_checkbox','')){?>
        <a class="button privacyPolicyBtn" id="pp_accept" href="<?php echo get_option('privacy_policy_page_link_option','');?>" target="<?php echo get_option('cookie_notice_link');  ?>"><?php echo get_option('enable_privacy_policy_link','Privacy Policy'); ?>
        </a>
        <?php } ?>
        <?php if(get_option('cn_refuse','')){?>
        <button class="refuse" id="refuse" onclick="myFunction()" style="margin-left:3px"><?php echo get_option('cookie_notice_refuse','Refuse'); ?>
        </button>
        
        
    	<?php } ?>
      </div>
</div>
<script>
   function myFunction() {
        $('.gdpr_container').remove();  
    };
</script>